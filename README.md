Версия с базой SQLite

#### cargo watch + tests

```console
cargo install cargo-watch
```
```console
cd <dir>
```
```console
cargo watch -q -c -w src/ -x 'test -- --test-threads=1 --nocapture'
```
Запуск с перезагрузкой на изменения.
```console
cargo watch -x run
```