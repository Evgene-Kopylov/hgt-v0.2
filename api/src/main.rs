#![allow(unused)]

mod users;

use users::User;
use warp::Filter;

#[tokio::main]
async fn main() {
    // APIs
    let root = warp::path::end()
        .map(|| "{\"Root_Api\":\"Live\"}".to_string());
    let hello = warp::path!("hello" / String / String)
        .map(|x, y| format!("Hello, {} {}!", x, y));

    let test_user = User::new("Test User");

    let user = warp::path!("user" / i64)
        .map(move |_| serde_json::to_string(&test_user).unwrap());

    // Routes
    let routes = root
        .or(hello)
        .or(user);

    warp::serve(routes)
        .run(([0, 0, 0, 0], 3030))
        .await;
}
