use uuid::{uuid, Uuid};
use serde::{Serialize, Deserialize};

#[derive(Debug, Clone)]
#[derive(Serialize, Deserialize)]
pub(crate) struct User {
    id: i64,
    uuid: Uuid,
    name: String,
    #[serde(skip_serializing)]
    _hidden_comment: String,
}

impl User {
    pub(crate) fn new(name: &str) -> User {
        User {
            id: 1,
            uuid: Uuid::new_v4(),
            name: name.to_string(),
            _hidden_comment: "Скрытое поле".to_string(),
        }
    }
}

